import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class Frame extends JFrame  {
    JTextField text1,text2;
    JButton b2;

    // mai intai se va scrie in textfield de jos mesajul si apoi se va selecta fisierul dorit

    // selectam textul din txt2
    Frame(){setTitle("Titlul ferestrei");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        init();
        setVisible(true);}


        public void init(){

            this.setLayout(null);
            int width=80;int height = 20;

            text1 = new JTextField();
            text1.setBounds(70,50,width, height);

            text2 = new JTextField();
            text2.setBounds(70,100,width, height);



            b2 = new JButton("Alege si scrie ");
            b2.setBounds(100,150,width, height);

            add(text1);
            add(text2);

            add(b2);

            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    final JFileChooser fc = new JFileChooser(); // am ales sa folosesc jFileChooser
                    int returnVal = fc.showOpenDialog(fc);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        String fileName = file.getName();
                        text1.setText(fileName);
                        String path=file.getPath();

                        try {
                            FileWriter myWriter = new FileWriter(path);
                            myWriter.write(text2.getText());
                            myWriter.close();
                            System.out.println("Am scris in fisier.");
                        } catch (IOException e) {
                            System.out.println("Eroare.");
                            e.printStackTrace();
                        }



                }}
            });




}



        public static void main(String[] args) {


        new Frame();


    }




}
